/**
 * Created by panteyenko on 11/30/2016.
 * Ввести с консоли три числа, подсчитать сумму квадратов двух наибольших чисел.
 * Результат вывести в консоль.
 */

import java.util.Scanner;

public class Task_2_1_1 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        //
//        Scanner in = new Scanner(System.in);
//        int[] nums = new int[5];
//        for (int i = 0; i < nums.length; i++) {
//            nums[i] = in.nextInt();
//        }
            int[] nums = {1, 7, 1, 5, 4, 3, 2, 6};
        int max1 = nums[0];
        int max2 = 0;
        int k = 0;

        for (int i = 0; i < nums.length; i++) {
            if (nums[i] > max1) {
                max1 = nums[i];
                k = i;
            }
            System.out.println(nums[i]);
        }
       // System.out.println(k);
        for (int i = 0; i < nums.length; i++) {
            if (i == k)
                continue;
            if (nums[i] > max2) {
                max2 = nums[i];
            }
        }
double result = (max1 - max2)*(max1+max2);
        System.out.println("max1 = " + max1);
        System.out.println("max2 = " + max2);
        System.out.println("result = " + result);

    }
}

