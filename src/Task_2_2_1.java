/**
 * Created by panteyenko on 11/30/2016.
 * Ввести с консоли три числа (a, b, c).
 * Решить квадратное уравнение ax^2 + bx + c.
 * Результат вывести в консоль.
 */
import java.util.Scanner;
public class Task_2_2_1 {

    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Scanner in = new Scanner(System.in);
        System.out.print("Введите a: ");
        int a = in.nextInt();
        System.out.print("Введите b: ");
        int b = in.nextInt();
        System.out.print("Введите c: ");
        int c = in.nextInt();
        System.out.print("Квадратное уравнение: " + a + "x^2 + " + b + "x + " + c + " = 0");
        System.out.println();

        if ((a == 0) || (b == 0) || (c == 0)) {
            System.out.print("Введите значения переменных отличные от нуля");
        }
            else {
            double D = (Math.pow(2, b) - 4*a*c)/2*a;
            System.out.println("D = " + D);

            if (D > 0){
double x1 = (- b - Math.sqrt(D))/(2*a);
double x2 = (- b + Math.sqrt(D))/(2*a);
                System.out.println("x1 = " + x1);
                System.out.println("x2 = " + x2);
            }
else if (D < 0){
                System.out.println("Корней нет");
            }
            else if (D == 0){
                double x1 = (- b - Math.sqrt(D))/(2*a);
                System.out.println("x1 = x2 = " + x1);
            }
            }
    }
}
